import helpers.Browser;
import helpers.DriverProvider;
import helpers.RandPartical;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.AdminMainPage;
import pages.LoginPage;
import pages.MainPage;

//import java.net.URL;
//import java.sql.Driver;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MatrixBoardTest {
    private WebDriver driver;
    private WebDriverWait wait;

    private String FirstName ;
    private String lastName  ;
    //private static final String URL = "http://at.pflb.ru/matrixboard2/";

    @BeforeTest
    public void start(){
        System.setProperty("webdriver.gecko.driver","/home/doublezero/.seleniumDrivers/geckodriver");
        driver = DriverProvider.getDriver(Browser.FIREFOX);
        wait = new WebDriverWait(driver,10 );
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);


    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test(priority=1)
    public void loginTest(){
        driver.get("http://at.pflb.ru/matrixboard2/");
        LoginPage loginPage = new LoginPage(driver);

        loginPage.fillLogin("admin");
        loginPage.fillPassword("admin");
        loginPage.submit();

        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(mainPage.getCurrentUsername(),"admin","Авторизация Провалена");

    }
    @Test(priority=2)
    public void isAdminPageTest() {
        AdminMainPage adminMainPage = new AdminMainPage(driver);
        Assert.assertTrue(adminMainPage.isAdminPage(),"The page is not an admin page.");
    }
    @Test(priority = 3)
    public void addUserFormAvailabilityTest(){
        AdminMainPage adminMainPage = new AdminMainPage(driver);
        adminMainPage.openAddPersonForm();
        Assert.assertTrue(adminMainPage.addPersonFormIsDisplayed(),"User create form is not available.");
    }
    @Test(priority = 4)
    public void addUserTest(){
        AdminMainPage adminMainPage = new AdminMainPage(driver);

        List<String> names;
        boolean hm;

         FirstName = "AHAGSFDGHDHGD" + RandPartical.getPartical();
         lastName = "HJGFHGFHGFGHKKG";
        //String particleName = "";
        while (adminMainPage.hasName(lastName + " " +FirstName)) {
            FirstName = "AHAGSFDGHDHGD" + RandPartical.getPartical();
        }
        adminMainPage.filPersonFirstNameInput(FirstName);
        adminMainPage.filPersonLastNameInput(lastName);
        adminMainPage.addPersonFromSubmit();

        hm = adminMainPage.hasName(lastName + " " +FirstName);
        Assert.assertTrue(hm,"There is no such name on the page.");

    }
    @Test(priority = 5)
    public void deleteUser(){
        AdminMainPage adminMainPage = new AdminMainPage(driver);
        String name = lastName + " " +FirstName;
        adminMainPage.deletePersonSubmit(name);
        adminMainPage.deletePersonFormSubmit();
        Assert.assertFalse(adminMainPage.hasName(lastName + " " +FirstName),"User was not deleted");
    }
    @Test(priority = 6)
    public void logout(){
        AdminMainPage adminMainPage = new AdminMainPage(driver);
        adminMainPage.logoutSubmit();
        WebElement copyright = driver.findElement(By.cssSelector("#copyright"));
        Assert.assertTrue(copyright.isDisplayed(),"User is logged out");
    }
}
