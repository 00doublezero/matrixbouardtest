package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class AdminMainPage extends AbstractPage {

    @FindBy(css="#logout span")
    private WebElement logoutButton;

    @FindBy(css="#add-person")
    private WebElement addPersonButton;

    @FindBy(css="div.ui-dialog:nth-child(3)")
    private WebElement addPersonFrom;

    @FindBy(css="#person-last-name")
    private WebElement personLastNameInput;

    @FindBy(css="#person-first-name")
    private WebElement personFirstNameInput;

    @FindBy(css="div.ui-dialog:nth-child(3) > div:nth-child(3) > div:nth-child(1) > button:nth-child(1)")
    private WebElement addPersonFromSubmitButton;

    @FindBy(className="name")
    private List<WebElement> userNames;

    @FindBy(css=".name > div:nth-child(2)")
    private WebElement deleteButton;

    @FindBy(css="div.ui-dialog:nth-child(4) > div:nth-child(3) > div:nth-child(1) > button:nth-child(1)")
    private WebElement deleteButtonFormOk;

    public AdminMainPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAdminPage() {
        return addPersonButton.isDisplayed();
    }

    public boolean addPersonFormIsDisplayed() {
        return addPersonFrom.isDisplayed();
    }

    public void openAddPersonForm() {
        addPersonButton.click();
    }

    public void filPersonFirstNameInput(String firstName) {
        personFirstNameInput.sendKeys(firstName);
    }
    public void filPersonLastNameInput(String lastName) {
        personLastNameInput.sendKeys(lastName);
    }
    public void addPersonFromSubmit(){
        addPersonFromSubmitButton.click();
    }
    public void logoutSubmit(){
        logoutButton.click();
    }
    public void deletePersonSubmit(String name) {
        //userNames = driver.findElements(By.cssSelector(".name"));
        for (WebElement username: userNames) {
            String currentName = username.getText();
            if (currentName.equals(name)){
               username.findElement(By.cssSelector("[id^=\"delete-\"]")).click();
            }
        }
        //deleteButton.click();
    }
    public void deletePersonFormSubmit() {
        deleteButtonFormOk.click();
    }

    public boolean hasName(String name){
        boolean result =false;
        for (WebElement username: userNames) {
            String currentName = username.getText();
            if (currentName.equals(name)){
                result = true;
            }
        }
        return result;
        //return
    }

}
