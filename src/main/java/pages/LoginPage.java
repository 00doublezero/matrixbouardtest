package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage {
    //private WebDriver driver;
    private static final String URL = "http://at.pflb.ru/matrixboard2/";

    @FindBy(css="#login-username")
    private WebElement loginField;

    @FindBy(css ="#login-password")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id='login-button']")
    private WebElement submitButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void fillLogin(String text) {
        loginField.sendKeys(text);
    }

    public void fillPassword(String text) {
        passwordField.sendKeys(text);
    }

    public void submit() {
        submitButton.click();
    }
    public static LoginPage openPage(WebDriver driver) {
        //driver.getCurrentUrl(URL);
        driver.get(URL);
        return new LoginPage(driver);
    }
    /*public void openPage (){
        driver.get(URL);
    }*/
}
