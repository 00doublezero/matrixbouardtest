package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sun.applet.Main;

public class MainPage extends AbstractPage {
    //private WebDriver driver;

    @FindBy(css="#profile span")
    private WebElement userNameContainer;

    public MainPage (WebDriver driver) {
        super(driver);
    }

    public String getCurrentUsername() {
        return userNameContainer.getText();
    }
}
