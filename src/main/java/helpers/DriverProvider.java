package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;
import java.util.Map;

public class DriverProvider {
    //private DriverProvider instnce;
    private static Map<Browser, WebDriver> driverMap = new HashMap<>();
    private DriverProvider() {


    }
    public static WebDriver getDriver(Browser browser) {
        WebDriver driver = driverMap.get(browser);
        if (driver == null) {
            switch (browser) {
                case IE:
                    driver = new InternetExplorerDriver();
                    break;
                case FIREFOX:
                    driver = new FirefoxDriver();
                    break;
                case CHROME:
                    driver = new ChromeDriver();
                    break;
            }
            driverMap.put(browser, driver);
        }
        return driver;
        /*if(browser.equals(Browser.FIREFOX)) {

        }*/

    }
}
